﻿var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing guessCircleCenter(p1, p2, p3) function", function(){
    
	var ps = 
	[
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
			{ x: 1n, y:-2n },
			{ x: 3n, y:-1n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 0n },
			{ x: 2n, y: 0n },
			{ x: 2n, y: 2n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x: 5n, y: 0n },
			{ x: 3n, y: 4n },
		]),
		new PointSet(
		1n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x: 1440695n, y: 4398240n },
			{ x:-2356805n, y:-3581760n },
			{ x: -293505n, y: 5943840n },
			{ x: 2850675n, y: 3141600n },
			{ x: 1116475n, y: 4687200n },
		])
	];

    it("Should return center of circle through points if they are not lying on line", function(done){
        expect(ps[0].guessCircleCenter({x:3n, y:3n}, {x:5n, y:4n}, {x:-1n, y:-3n})).to.eql({x:-130n, y:76n});
        expect(ps[1].guessCircleCenter({x:0n, y:0n}, {x:2n, y:0n}, {x:2n, y:2n})).to.eql({x:8n, y:8n});
        expect(ps[2].guessCircleCenter({x:0n, y:5n}, {x:5n, y:0n}, {x:3n, y:4n})).to.eql({x:0n, y:0n});
        expect(ps[3].guessCircleCenter({x:6375625n, y:0n}, {x:-6375625n, y:0n}, {x:-2356805n, y:-3581760n})).to.eql({x:0n, y:283907341670793750000n});
        done();
    });
	
	it("Should return null if points are lying on line", function(done){
        expect(ps[0].guessCircleCenter({x:-3n, y:0n}, {x:-1n, y:1n}, {x:3n, y:3n})).to.eql(null);
        expect(ps[3].guessCircleCenter({x:-293505n, y:5943840n}, {x:2850675n, y:3141600n}, {x:1116475n, y:4687200n})).to.eql(null);
        done();
    });
	
});

