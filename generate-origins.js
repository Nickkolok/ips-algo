'use strict';

// Usage:
// nodejs generate-origins.js X Y
// Generates origins for diameters from X to Y (including X and Y)

const PointSetGraphFactory = require('./PointSetGraphFactory');
const fs = require('fs');
const JSON = require('bigint-json-native');

for(var d = process.argv[2]; d <= process.argv[3]; d++){
    console.log('Processing diameter', d);
    var t2 = Date.now();
    var psf = new PointSetGraphFactory();
    psf.dontAppendCopiesInAdvance = true;
    psf.makePointSetGraphs(d,d);
    console.log("Generating points, ms:", Date.now()-t2);
    console.log("Different chars before filtering", Object.keys(psf.graphs).length);
    for(let j = 7; j <= 7; j++){
        t2 = Date.now();
        psf.filterQuadTrivialsNoFacher();
        psf.filterAll(j);
        console.log("Different chars for "+j, Object.keys(psf.graphs).length);
        var str = JSON.stringify(psf);
        console.log('Dump length:', str.length);
        console.log("Filtering points, ms:", Date.now()-t2);
        fs.writeFileSync('aux/'+j+'_'+d+'_origin_nofacher0.json',str);
    }
}



/*
var t2 = Date.now();
var psf = new PointSetGraphFactory();
psf.dontAppendCopiesInAdvance = true;
var d = 500;
psf.makePointSetGraphs(d,d);
console.log("Different chars before filtering", Object.keys(psf.graphs).length);
console.log("Generating points, ms:", Date.now()-t2);
psf.filterAll(8);
console.log("Different chars after filtering", Object.keys(psf.graphs).length);
*/
/*
var psf = new PointSetGraphFactory();
psf.readFromFileSync('aux/7_19_origin.json');
console.log("Different chars before filtering", Object.keys(psf.graphs).length);
psf.filterAll(8);
console.log("Different chars after filtering", Object.keys(psf.graphs).length);
*/
