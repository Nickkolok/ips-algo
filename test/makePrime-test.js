var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing makePrime() function", function(){
    
	var ps = 
	[
		new PointSet(
		1n,
		1n,
		[
			{x:  4n, y: -3n},
			{x: 28n, y: -3n},
			{x:  4n, y: 15n},
		]),
		new PointSet(
		3n,
		1n,
		[
			{x:-3n, y: 1n},
			{x: 5n, y: 1n},
			{x: 1n, y: 5n},
		]),
	];
    var ex_ps = 
	[
		new PointSet(
		1n,
		1n,
		[
			{x: 0n, y: 0n},
			{x: 4n, y: 0n},
			{x: 0n, y: 3n},
		]),
		new PointSet(
		3n,
		2n,
		[
			{x: 0n, y: 0n},
			{x: 2n, y: 0n},
			{x: 1n, y: 1n},
		]),
		
	];

    it("Should return true for right reduced IPS", function(done){
        expect(ps[0].makePrime()).to.eql(ex_ps[0]);
        expect(ps[1].makePrime()).to.eql(ex_ps[1]);
        done();
    });
	
});
