﻿var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing inLineAndOutOfLine(h,k) function", function(){
    
	var ps = 
	[
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
			{ x: 1n, y:-2n },
			{ x: 3n, y:-1n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x:-2n, y:-2n },
			{ x: 0n, y: 2n },
			{ x: 3n, y: 5n },
			{ x: 1n, y: 4n },
			{ x: 4n, y: 2n },
			{ x: 5n, y:-1n },
			{ x:-1n, y: 0n },
			{ x: 6n, y:-4n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
			{ x: 1n, y:-2n },
			{ x: 3n, y:-1n },
			{ x: 1n, y:-1n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-1n, y:-2n },
			{ x: 0n, y: 0n },
			{ x:-2n, y:-4n },
			{ x: 1n, y: 2n },
			{ x: 3n, y:-4n },
			{ x: 2n, y:-1n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x:-1n, y:-2n },
			{ x: 0n, y: 0n },
			{ x:-2n, y:-4n },
			{ x: 1n, y: 2n },
			{ x: 3n, y:-4n },
			{ x: 2n, y:-1n },
			{ x: 4n, y: 3n },
		]),
	];

    it("Should return rigth arrays of points lying on line through given points and not lying on it", function(done){
        expect(ps[0].inLineAndOutOfLine( 0, 1 )).to.eql( [ [ 0, 1, 2, 3 ], [ 4, 5, 6 ] ] );
        expect(ps[0].inLineAndOutOfLine( 4, 5 )).to.eql( [ [ 4, 5, 6 ], [ 0, 1, 2, 3 ] ] );
        expect(ps[0].inLineAndOutOfLine( 4, 2 )).to.eql( [ [ 2, 4 ], [ 0, 1, 3, 5, 6 ] ] );
        expect(ps[1].inLineAndOutOfLine( 0, 1 )).to.eql( [ [ 0, 1, 3, 6 ], [ 2, 4, 5, 7 ] ] );
        expect(ps[1].inLineAndOutOfLine( 4, 2 )).to.eql( [ [ 2, 4, 5, 7 ], [ 0, 1, 3, 6 ] ] );
        expect(ps[1].inLineAndOutOfLine( 4, 1 )).to.eql( [ [ 1, 4 ], [ 0, 2, 3, 5, 6, 7 ] ] );
        expect(ps[2].inLineAndOutOfLine( 0, 1 )).to.eql( [ [ 0, 1, 2, 3 ], [ 4, 5, 6, 7 ] ] );
        expect(ps[2].inLineAndOutOfLine( 4, 5 )).to.eql( [ [ 4, 5, 6 ], [ 0, 1, 2, 3, 7 ] ] );
        expect(ps[2].inLineAndOutOfLine( 4, 2 )).to.eql( [ [ 2, 4 ], [ 0, 1, 3, 5, 6, 7 ] ] );
        expect(ps[3].inLineAndOutOfLine( 4, 0 )).to.eql( [ [ 0, 4, 5, 6 ], [ 1, 2, 3 ] ] );
        expect(ps[3].inLineAndOutOfLine( 1, 2 )).to.eql( [ [ 1, 2, 3, 4 ], [ 0, 5, 6 ] ] );
        expect(ps[3].inLineAndOutOfLine( 1, 0 )).to.eql( [ [ 0, 1 ], [ 2, 3, 4, 5, 6 ] ] );
        expect(ps[4].inLineAndOutOfLine( 4, 0 )).to.eql( [ [ 0, 4, 5, 6 ], [ 1, 2, 3, 7 ] ] );
        expect(ps[4].inLineAndOutOfLine( 1, 2 )).to.eql( [ [ 1, 2, 3, 4 ], [ 0, 5, 6, 7 ] ] );
        expect(ps[4].inLineAndOutOfLine( 1, 0 )).to.eql( [ [ 0, 1 ], [ 2, 3, 4, 5, 6, 7 ] ] );
        done();
    });
	
});

