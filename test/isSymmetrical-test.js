﻿var mocha  = require('mocha')
  , assert = require('chai').assert
  , expect = require('chai').expect
  , PointSet = require('../PointSet.js')
  ;

describe("Testing isSymmetrical() function", function(){
    
	var ps = 
	[
		new PointSet(//symmetrical
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 4n, y: 3n },
			{ x: 5n, y:-4n },
			{ x: 1n, y: 1n },
			{ x:-4n, y: 3n },
			{ x: 3n, y: 0n },
			{ x:-5n, y:-4n },
		]),
		new PointSet(//symmetrical with point on Y
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 4n, y: 3n },
			{ x: 5n, y:-4n },
			{ x: 1n, y: 1n },
			{ x: 0n, y: 0n },
			{ x:-4n, y: 3n },
			{ x: 3n, y: 0n },
			{ x:-5n, y:-4n },
		]),
		new PointSet(//not symmetrical
		1n,
		1n,
		[
			{ x:-3n, y: 0n },
			{ x:-1n, y: 1n },
			{ x: 3n, y: 3n },
			{ x: 5n, y: 4n },
			{ x:-1n, y:-3n },
			{ x: 1n, y:-2n },
			{ x: 3n, y:-1n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 0n },
			{ x: 2n, y: 0n },
			{ x: 2n, y: 2n },
		]),
		new PointSet(
		1n,
		1n,
		[
			{ x: 0n, y: 5n },
			{ x: 5n, y: 0n },
			{ x: 3n, y: 4n },
		]),
		new PointSet(
		1n,
		5050n,
		[
			{ x: 6375625n, y:       0n },
			{ x:-6375625n, y:       0n },
			{ x: 1440695n, y: 4398240n },
			{ x:-2356805n, y:-3581760n },
			{ x: -293505n, y: 5943840n },
			{ x: 2850675n, y: 3141600n },
			{ x: 1116475n, y: 4687200n },
		])
	];

	it("Should return true if PS is symmetrical about Y-axis", function(done){
		expect(ps[0].isSymmetrical()).to.eql(true);
		expect(ps[1].isSymmetrical()).to.eql(true);
	
	
		done();
	});
	
	it("Should return false if PS is not symmetrical about Y-axis", function(done){
		expect(ps[2].isSymmetrical()).to.eql(false);
		expect(ps[3].isSymmetrical()).to.eql(false);
		expect(ps[4].isSymmetrical()).to.eql(false);
		expect(ps[5].isSymmetrical()).to.eql(false);
	
	
		done();
	});
	
});

