// Drafts of the maximizer

const PointSet = require('./PointSet');
const isFullSquare = require('bigint-is-perfect-square');
const isqrt = require('bigint-isqrt');

let test1 = new PointSet(
	1n,
	1n,
	[
		{x:0n, y:0n},
		{x:0n, y:4n},
		{x:3n, y:0n},
	]
);


test1 = new PointSet(
	15n,
	2n,
	[
		{x:-4n,y:0n},
		{x: 4n,y:0n},
		{x: 3n,y:1n},
		{x: -3n,y:1n},
	]
);


test1 = new PointSet(
		2002n,
		2227n,
		[
			{ x: 17615968n, y: 238464n  },
			{ x: 7344908n,  y: 411864n  },
			{ x: 19079044n, y:-54168n   },
			{ x: 0n,        y: 0n       },
			{ x: 49595290n, y: 0n       },
			{ x: 26127018n, y: 932064n  },
			{ x: 32142553n, y: 411864n  },
		])

test1.shift(-test1.p[0].x,-test1.p[0].y);
test1.rotateBig(1);
test1.reduce();

//test1.stretch(2n);
console.log(test1);


let xA = test1.p[2].x;
let yA = test1.p[2].y;
let xB = test1.p[1].x;
let q = test1.char;

let candidates=[];

let m_max = test1.distance(test1.p[2],test1.p[0]);
let n_max = test1.distance(test1.p[1],test1.p[0]);

console.log(test1.distanceMatrix());

for(let n = -n_max; n <=n_max; n+=test1.base){
	console.log('n:', n);

	let k2 = -2n*n*q*yA;


	for(let m = -m_max; m <=m_max; m+=test1.base){
		//console.log('m,n:', m,n);


		let k3 = -2n*m*xB+2n*n*xA;
		let k1 = (-1n)*m**2n*n+m*n**2n+n*xA**2n-m*xB**2n+n*q*yA**2n;


		if(k3 === 0n){
			//console.log('Division by zero!');
			continue;
		}


		let k6 =
			(k2**2n+k3**2n*q)*xB**4n-
			k1*k3*4n*q*xB**3n+
			((4n*k1**2n-2n*k3**2n*n**2n)*q-2n*k2**2n*n**2n)*xB**2n+4n*k1*k3*n**2n*q*xB+(k3**2n*n**4n-4n*k1**2n*n**2n)*q+k2**2n*n**4n;



		if(k6 < 0n){
			//console.log('Negative discriminant - no solutions:',m,n);
			//break;
			continue;
		}


		if(!isFullSquare(k6)){
			//console.log('Wrong characteristic ' + k6);
			continue;
		}


		sqrt_k6 = isqrt(k6);


		let k4 = 2n*k2**2n*xB**2n-2n*k3**2n*n**2n*q-2n*k2**2n*n**2n;

		if(k4 === 0n){
			console.log('Division by zero!');
			continue;
		}


		let k5 = 2n*k1*k2*n**2n-k2*k3*n**2n*xB-2n*k1*k2*xB**2n+k2*k3*xB**3n;



		let y_k4;
		y_k4 = k5 + k3 * n * sqrt_k6;
		if(y_k4 % k4 === 0n){
			let y = y_k4/k4;
			let x_k3 = (k1+k2*y);
			if(x_k3 % k3 !== 0n){
				console.log('x is not on the lattice - weird denominator, possible usable after a stretch');
				continue;
			}
			let x = x_k3 / k3;
			console.log('Looks good:', x, y);
			candidates.push([x,y]);

		}else{
			console.log('y+ is not on the lattice - weird denominator, possible usable after a stretch: ' + m + '  ' + n);
			//continue;
		}

		y_k4 = k5 - k3 * n * sqrt_k6;
		if(y_k4 % k4 === 0n){
			let y = y_k4/k4;
			let x_k3 = (k1+k2*y);
			if(x_k3 % k3 !== 0n){
				console.log('x is not on the lattice - weird denominator, possible usable after a stretch');
				continue;
			}
			let x = x_k3 / k3;
			console.log('Looks good:', x, y);
			candidates.push([x,y]);
		}else{
			console.log('y- is not on the lattice - weird denominator, possible usable after a stretch: ' + m + '  ' + n);
			//continue;
		}


	}
}

console.log(test1);

console.log(candidates);
