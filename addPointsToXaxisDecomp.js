'use strict';
// Usage: node addPointsToXaxis.js filename.json steps stretchCoeff

const fs = require('fs');
const Performance = require('perf_hooks').performance;
const JSON = require('bigint-json-native')({"BigNumber": BigInt, "noNew": true, "forceBig": true});
const PointSet = require('./PointSet');
const PrimeFactorization = require('./PrimeFactorization');
const classifyAndWrite = require('./classifyAndWrite');

//console.log(process.argv);
let filename = process.argv[2];
let stretchCoeff = BigInt(process.argv[3]);

let ps = new PointSet();
ps.fromJSON(fs.readFileSync(filename,'utf8'));

// Stretching the PointSet

ps.stretch(stretchCoeff);
ps.base /= stretchCoeff;

ps.reduce();

//console.log(ps);

// Let us found the first point not on X axis
let apex = null;
for(let point of ps.p){
    if(point.y !== 0n){
        apex = point;
        break;
    }
}

if(apex === null){
    console.log('No points out of X axis, is the data correct?');
    return;
}

ps.shift(-apex.x,0n);

let line = ps.deepClone();
line.p = line.p.filter(p => (p.y === 0n));

let outOfLine = ps.deepClone();
outOfLine.p = outOfLine.p.filter(p => (p.y !== 0n)).reverse(/*make apex the last*/);


let hSquare = ps.char*(apex.y**2n);

let pf = new PrimeFactorization(hSquare);

let t0 = Performance.now();

pf.enumerateDivisors(1n,function(divisor){
    let P = divisor;
    let Q = hSquare/divisor;
    tryCandidate({x:ps.base*(P-Q)/2n,y:0n});
});

console.log("Time spent, ms: " + (Performance.now() - t0) );

function tryCandidate(candidate){
    for(let point of line.p){
        if(point.x === candidate.x ){
            return;
        }
    }

    for(let point of outOfLine.p){
        if(!outOfLine.isDistanceIntegral(point,candidate)){
            return;
        }
    }


    ps.p.push(candidate);
    console.log(ps.p.length);
    classifyAndWrite(ps);
}

/*
let stepsDone = 0n;
for(let x = shift - ps.base * steps; x <= shift + ps.base * steps; x += ps.base){
    // TODO: fastenate
    let candidate = {x: x, y: 0n};
    let good = true;
    for(let point of ps.p){
        if((point.x === candidate.x) || (point.y && !ps.isDistanceIntegral(point,candidate)) ){
            good = false;
            break;
        }
    }
    if(good){
        ps.p.push(candidate);
        console.log(ps.p.length);
		classifyAndWrite(ps);
    }
    stepsDone++;
    if(stepsDone % 100000n === 0n){
		console.log(stepsDone + ' of ' + 2n*steps);
	}
}
*/
console.log('Final length:', ps.p.length);
classifyAndWrite(ps,{symm:true});
